package ru.ekfedorov.tm.exception.system;

import ru.ekfedorov.tm.exception.AbstractException;

public class UserIsLocked extends AbstractException {

    public UserIsLocked() throws Exception {
        super("The user is locked...");
    }

}
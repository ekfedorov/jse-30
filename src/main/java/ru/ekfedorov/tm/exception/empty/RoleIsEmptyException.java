package ru.ekfedorov.tm.exception.empty;

import ru.ekfedorov.tm.exception.AbstractException;

public final class RoleIsEmptyException extends AbstractException {

    public RoleIsEmptyException() throws Exception {
        super("Error! Role is empty...");
    }

}

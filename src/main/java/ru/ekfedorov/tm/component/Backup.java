package ru.ekfedorov.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.ekfedorov.tm.bootstrap.Bootstrap;
import ru.ekfedorov.tm.command.data.BackupLoadCommand;
import ru.ekfedorov.tm.command.data.BackupSaveCommand;

public class Backup extends Thread {

    private static final int INTERVAL = 15000;

    @NotNull
    public final Bootstrap bootstrap;

    public Backup(@NotNull Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
        setDaemon(true);
    }

    public void init() {
        load();
        start();
    }

    public void load() {
        bootstrap.parseCommand(BackupLoadCommand.BACKUP_LOAD);
    }

    @Override
    @SneakyThrows
    public void run() {
        while (true) {
            save();
            Thread.sleep(INTERVAL);
        }
    }

    public void save() {
        bootstrap.parseCommand(BackupSaveCommand.BACKUP_SAVE);
    }

}

package ru.ekfedorov.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.dto.Domain;
import ru.ekfedorov.tm.enumerated.Role;

import java.nio.file.Files;
import java.nio.file.Paths;

public class DataXmlLoadFasterXmlCommand extends AbstractDataCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Load XML(fasterxml) data.";
    }

    @NotNull
    @Override
    public String commandName() {
        return "data-xml-fasterxml-load";
    }

    @NotNull
    public Role[] commandRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA XML LOAD]");
        @NotNull final String xml = new String(Files.readAllBytes(Paths.get(FILE_FASTERXML_XML)));
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        @NotNull final Domain domain = objectMapper.readValue(xml, Domain.class);
        setDomain(domain);
    }

}


package ru.ekfedorov.tm.command.project;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.command.AbstractProjectCommand;
import ru.ekfedorov.tm.exception.system.NullObjectException;

public final class ProjectClearAllCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Clear all project.";
    }

    @NotNull
    @Override
    public String commandName() {
        return "project-clear";
    }

    @SneakyThrows
    @Override
    public void execute() {
        if (serviceLocator == null) throw new NullObjectException();
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[CLEAR]");
        serviceLocator.getProjectService().clear(userId);
        System.out.println("--- successfully cleared ---\n");
    }

}
